/*
Copyright 2022 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	"fmt"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	// JJJ Juanjo: comento estas 3 líneas, porque no quiero que se cargue el controller-runtime
	// (provocará errores con nuestro base-controller, porque inicializa flags)
	// ctrl "sigs.k8s.io/controller-runtime"
	// "sigs.k8s.io/controller-runtime/pkg/webhook"
	// "sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// JJJ Juanjo: ídem, comento por el controller-runtime
// // log is for logging in this package.
// var _ = ctrl.Log.WithName("awsclusterstaticidentity-resource")

// JJJ Juanjo: ídem, comento por el controller-runtime
// func (r *AWSClusterStaticIdentity) SetupWebhookWithManager(mgr ctrl.Manager) error {
// 	return ctrl.NewWebhookManagedBy(mgr).
// 		For(r).
// 		Complete()
// }

// +kubebuilder:webhook:verbs=create;update,path=/validate-infrastructure-cluster-x-k8s-io-v1beta2-awsclusterstaticidentity,mutating=false,failurePolicy=fail,matchPolicy=Equivalent,groups=infrastructure.cluster.x-k8s.io,resources=awsclusterstaticidentities,versions=v1beta2,name=validation.awsclusterstaticidentity.infrastructure.cluster.x-k8s.io,sideEffects=None,admissionReviewVersions=v1;v1beta1
// +kubebuilder:webhook:verbs=create;update,path=/mutate-infrastructure-cluster-x-k8s-io-v1beta2-awsclusterstaticidentity,mutating=true,failurePolicy=fail,matchPolicy=Equivalent,groups=infrastructure.cluster.x-k8s.io,resources=awsclusterstaticidentities,versions=v1beta2,name=default.awsclusterstaticidentity.infrastructure.cluster.x-k8s.io,sideEffects=None,admissionReviewVersions=v1;v1beta1

// JJJ Juanjo: ídem, comento por el controller-runtime
// var (
// 	_ webhook.Validator = &AWSClusterStaticIdentity{}
// 	_ webhook.Defaulter = &AWSClusterStaticIdentity{}
// )

// JJJ Juanjo
// He cambiado todas las ocurrencias de "admission.Warnings" por "jjjWarnings"

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type.
func (r *AWSClusterStaticIdentity) ValidateCreate() (jjjWarnings, error) {
	// Validate selector parses as Selector
	if r.Spec.AllowedNamespaces != nil {
		_, err := metav1.LabelSelectorAsSelector(&r.Spec.AllowedNamespaces.Selector)
		if err != nil {
			return nil, field.Invalid(field.NewPath("spec", "allowedNamespaces", "selector"), r.Spec.AllowedNamespaces.Selector, err.Error())
		}
	}

	return nil, nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type.
func (r *AWSClusterStaticIdentity) ValidateDelete() (jjjWarnings, error) {
	return nil, nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type.
func (r *AWSClusterStaticIdentity) ValidateUpdate(old runtime.Object) (jjjWarnings, error) {
	oldP, ok := old.(*AWSClusterStaticIdentity)
	if !ok {
		return nil, apierrors.NewBadRequest(fmt.Sprintf("expected an AWSClusterStaticIdentity but got a %T", old))
	}

	if oldP.Spec.SecretRef != r.Spec.SecretRef {
		return nil, field.Invalid(field.NewPath("spec", "secretRef"),
			r.Spec.SecretRef, "field cannot be updated")
	}

	// Validate selector parses as Selector
	if r.Spec.AllowedNamespaces != nil {
		_, err := metav1.LabelSelectorAsSelector(&r.Spec.AllowedNamespaces.Selector)
		if err != nil {
			return nil, field.Invalid(field.NewPath("spec", "allowedNamespaces", "selector"), r.Spec.AllowedNamespaces.Selector, err.Error())
		}
	}

	return nil, nil
}

// Default should return the default AWSClusterStaticIdentity.
func (r *AWSClusterStaticIdentity) Default() {
	SetDefaults_Labels(&r.ObjectMeta)
}

/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1beta2

import (
	v1beta2 "gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_aws/aws/v1beta2"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// AWSClusterRoleIdentityLister helps list AWSClusterRoleIdentities.
// All objects returned here must be treated as read-only.
type AWSClusterRoleIdentityLister interface {
	// List lists all AWSClusterRoleIdentities in the indexer.
	// Objects returned here must be treated as read-only.
	List(selector labels.Selector) (ret []*v1beta2.AWSClusterRoleIdentity, err error)
	// AWSClusterRoleIdentities returns an object that can list and get AWSClusterRoleIdentities.
	AWSClusterRoleIdentities(namespace string) AWSClusterRoleIdentityNamespaceLister
	AWSClusterRoleIdentityListerExpansion
}

// aWSClusterRoleIdentityLister implements the AWSClusterRoleIdentityLister interface.
type aWSClusterRoleIdentityLister struct {
	indexer cache.Indexer
}

// NewAWSClusterRoleIdentityLister returns a new AWSClusterRoleIdentityLister.
func NewAWSClusterRoleIdentityLister(indexer cache.Indexer) AWSClusterRoleIdentityLister {
	return &aWSClusterRoleIdentityLister{indexer: indexer}
}

// List lists all AWSClusterRoleIdentities in the indexer.
func (s *aWSClusterRoleIdentityLister) List(selector labels.Selector) (ret []*v1beta2.AWSClusterRoleIdentity, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1beta2.AWSClusterRoleIdentity))
	})
	return ret, err
}

// AWSClusterRoleIdentities returns an object that can list and get AWSClusterRoleIdentities.
func (s *aWSClusterRoleIdentityLister) AWSClusterRoleIdentities(namespace string) AWSClusterRoleIdentityNamespaceLister {
	return aWSClusterRoleIdentityNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// AWSClusterRoleIdentityNamespaceLister helps list and get AWSClusterRoleIdentities.
// All objects returned here must be treated as read-only.
type AWSClusterRoleIdentityNamespaceLister interface {
	// List lists all AWSClusterRoleIdentities in the indexer for a given namespace.
	// Objects returned here must be treated as read-only.
	List(selector labels.Selector) (ret []*v1beta2.AWSClusterRoleIdentity, err error)
	// Get retrieves the AWSClusterRoleIdentity from the indexer for a given namespace and name.
	// Objects returned here must be treated as read-only.
	Get(name string) (*v1beta2.AWSClusterRoleIdentity, error)
	AWSClusterRoleIdentityNamespaceListerExpansion
}

// aWSClusterRoleIdentityNamespaceLister implements the AWSClusterRoleIdentityNamespaceLister
// interface.
type aWSClusterRoleIdentityNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all AWSClusterRoleIdentities in the indexer for a given namespace.
func (s aWSClusterRoleIdentityNamespaceLister) List(selector labels.Selector) (ret []*v1beta2.AWSClusterRoleIdentity, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1beta2.AWSClusterRoleIdentity))
	})
	return ret, err
}

// Get retrieves the AWSClusterRoleIdentity from the indexer for a given namespace and name.
func (s aWSClusterRoleIdentityNamespaceLister) Get(name string) (*v1beta2.AWSClusterRoleIdentity, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1beta2.Resource("awsclusterroleidentity"), name)
	}
	return obj.(*v1beta2.AWSClusterRoleIdentity), nil
}

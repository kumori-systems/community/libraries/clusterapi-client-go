/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	"context"

	v1beta2 "gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_aws/aws/v1beta2"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeAWSClusterStaticIdentities implements AWSClusterStaticIdentityInterface
type FakeAWSClusterStaticIdentities struct {
	Fake *FakeInfrastructureV1beta2
}

var awsclusterstaticidentitiesResource = schema.GroupVersionResource{Group: "infrastructure.cluster.x-k8s.io", Version: "v1beta2", Resource: "awsclusterstaticidentities"}

var awsclusterstaticidentitiesKind = schema.GroupVersionKind{Group: "infrastructure.cluster.x-k8s.io", Version: "v1beta2", Kind: "AWSClusterStaticIdentity"}

// Get takes name of the aWSClusterStaticIdentity, and returns the corresponding aWSClusterStaticIdentity object, and an error if there is any.
func (c *FakeAWSClusterStaticIdentities) Get(ctx context.Context, name string, options v1.GetOptions) (result *v1beta2.AWSClusterStaticIdentity, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(awsclusterstaticidentitiesResource, name), &v1beta2.AWSClusterStaticIdentity{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1beta2.AWSClusterStaticIdentity), err
}

// List takes label and field selectors, and returns the list of AWSClusterStaticIdentities that match those selectors.
func (c *FakeAWSClusterStaticIdentities) List(ctx context.Context, opts v1.ListOptions) (result *v1beta2.AWSClusterStaticIdentityList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(awsclusterstaticidentitiesResource, awsclusterstaticidentitiesKind, opts), &v1beta2.AWSClusterStaticIdentityList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1beta2.AWSClusterStaticIdentityList{ListMeta: obj.(*v1beta2.AWSClusterStaticIdentityList).ListMeta}
	for _, item := range obj.(*v1beta2.AWSClusterStaticIdentityList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested aWSClusterStaticIdentities.
func (c *FakeAWSClusterStaticIdentities) Watch(ctx context.Context, opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(awsclusterstaticidentitiesResource, opts))
}

// Create takes the representation of a aWSClusterStaticIdentity and creates it.  Returns the server's representation of the aWSClusterStaticIdentity, and an error, if there is any.
func (c *FakeAWSClusterStaticIdentities) Create(ctx context.Context, aWSClusterStaticIdentity *v1beta2.AWSClusterStaticIdentity, opts v1.CreateOptions) (result *v1beta2.AWSClusterStaticIdentity, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(awsclusterstaticidentitiesResource, aWSClusterStaticIdentity), &v1beta2.AWSClusterStaticIdentity{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1beta2.AWSClusterStaticIdentity), err
}

// Update takes the representation of a aWSClusterStaticIdentity and updates it. Returns the server's representation of the aWSClusterStaticIdentity, and an error, if there is any.
func (c *FakeAWSClusterStaticIdentities) Update(ctx context.Context, aWSClusterStaticIdentity *v1beta2.AWSClusterStaticIdentity, opts v1.UpdateOptions) (result *v1beta2.AWSClusterStaticIdentity, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(awsclusterstaticidentitiesResource, aWSClusterStaticIdentity), &v1beta2.AWSClusterStaticIdentity{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1beta2.AWSClusterStaticIdentity), err
}

// Delete takes name of the aWSClusterStaticIdentity and deletes it. Returns an error if one occurs.
func (c *FakeAWSClusterStaticIdentities) Delete(ctx context.Context, name string, opts v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteActionWithOptions(awsclusterstaticidentitiesResource, name, opts), &v1beta2.AWSClusterStaticIdentity{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeAWSClusterStaticIdentities) DeleteCollection(ctx context.Context, opts v1.DeleteOptions, listOpts v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(awsclusterstaticidentitiesResource, listOpts)

	_, err := c.Fake.Invokes(action, &v1beta2.AWSClusterStaticIdentityList{})
	return err
}

// Patch applies the patch and returns the patched aWSClusterStaticIdentity.
func (c *FakeAWSClusterStaticIdentities) Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts v1.PatchOptions, subresources ...string) (result *v1beta2.AWSClusterStaticIdentity, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(awsclusterstaticidentitiesResource, name, pt, data, subresources...), &v1beta2.AWSClusterStaticIdentity{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1beta2.AWSClusterStaticIdentity), err
}

/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1beta1

// OpenStackClusterListerExpansion allows custom methods to be added to
// OpenStackClusterLister.
type OpenStackClusterListerExpansion interface{}

// OpenStackClusterNamespaceListerExpansion allows custom methods to be added to
// OpenStackClusterNamespaceLister.
type OpenStackClusterNamespaceListerExpansion interface{}

// OpenStackClusterTemplateListerExpansion allows custom methods to be added to
// OpenStackClusterTemplateLister.
type OpenStackClusterTemplateListerExpansion interface{}

// OpenStackClusterTemplateNamespaceListerExpansion allows custom methods to be added to
// OpenStackClusterTemplateNamespaceLister.
type OpenStackClusterTemplateNamespaceListerExpansion interface{}

// OpenStackMachineListerExpansion allows custom methods to be added to
// OpenStackMachineLister.
type OpenStackMachineListerExpansion interface{}

// OpenStackMachineNamespaceListerExpansion allows custom methods to be added to
// OpenStackMachineNamespaceLister.
type OpenStackMachineNamespaceListerExpansion interface{}

// OpenStackMachineTemplateListerExpansion allows custom methods to be added to
// OpenStackMachineTemplateLister.
type OpenStackMachineTemplateListerExpansion interface{}

// OpenStackMachineTemplateNamespaceListerExpansion allows custom methods to be added to
// OpenStackMachineTemplateNamespaceLister.
type OpenStackMachineTemplateNamespaceListerExpansion interface{}

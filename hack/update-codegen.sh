#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

BASE_PATH=$(dirname "${BASH_SOURCE[0]}")/..

# ------------------------------------------------------------------------------
#
# BE CAREFUL, SELECT THE RIGHT LOCATION OF CODE-GENERATOR PACKAGE
#
# ------------------------------------------------------------------------------
CODEGEN_PKG=$GOPATH/pkg/mod/k8s.io/code-generator@v0.25.6


# ------------------------------------------------------------------------------
# JValero:
#
# The generator should run with something like this ...
#
#   "${CODEGEN_PKG}"/generate-groups.sh \
#     all \
#     pkg/generated \
#     client-go/pkg/apis \
#     kumori:v1 \
#     --output-base "${BASE_PATH}/.." \
#     --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt
#
# ... but I doesnt works for me, so I had splitted it in two parts and move
# directories !!
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# clusterapi/v1beta1
# ------------------------------------------------------------------------------

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-clusterapi \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_clusterapi \
  clusterapi:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated-clusterapi
mkdir $BASE_PATH/pkg/generated-clusterapi
mv $BASE_PATH/gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-clusterapi/* $BASE_PATH/pkg/generated-clusterapi
rm -rf $BASE_PATH/gitlab.com

# Generating just the deepcopy function
"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis_clusterapi" \
  clusterapi:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# ------------------------------------------------------------------------------
# bootstrap/v1beta1
# ------------------------------------------------------------------------------

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-bootstrap \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_bootstrap \
  bootstrap:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated-bootstrap
mkdir $BASE_PATH/pkg/generated-bootstrap
mv $BASE_PATH/gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-bootstrap/* $BASE_PATH/pkg/generated-bootstrap
rm -rf $BASE_PATH/gitlab.com

# Generating just the deepcopy function
"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis_bootstrap" \
  bootstrap:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# ------------------------------------------------------------------------------
# controlplane/v1beta1
# ------------------------------------------------------------------------------

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-controlplane \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_controlplane \
  controlplane:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated-controlplane
mkdir $BASE_PATH/pkg/generated-controlplane
mv $BASE_PATH/gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-controlplane/* $BASE_PATH/pkg/generated-controlplane
rm -rf $BASE_PATH/gitlab.com

# Generating just the deepcopy function
"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis_controlplane" \
  controlplane:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# ------------------------------------------------------------------------------
# openstack/v1beta1
# ------------------------------------------------------------------------------

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-openstack \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_openstack \
  openstack:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated-openstack
mkdir $BASE_PATH/pkg/generated-openstack
mv $BASE_PATH/gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-openstack/* $BASE_PATH/pkg/generated-openstack
rm -rf $BASE_PATH/gitlab.com

# Generating just the deepcopy function
"${CODEGEN_PKG}"/generate-groups.sh \
  deepcopy-gen \
  this_is_not_used \
  "${BASE_PATH}/pkg/apis_openstack" \
  openstack:v1beta1 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# ------------------------------------------------------------------------------
# aws/v1beta2
# ------------------------------------------------------------------------------

# Generating informer, listers and clients
"${CODEGEN_PKG}"/generate-groups.sh \
  defaulter-gen,client-gen,lister-gen,informer-gen \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-aws \
  gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/apis_aws \
  aws:v1beta2 \
  --output-base "${BASE_PATH}" \
  --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt

# Moving the generated code to the right directory:
# (I am not able to generate rigth code into the righ directory)
rm -rf $BASE_PATH/pkg/generated-aws
mkdir $BASE_PATH/pkg/generated-aws
mv $BASE_PATH/gitlab.com/kumori-systems/community/libraries/clusterapi-client-go/pkg/generated-aws/* $BASE_PATH/pkg/generated-aws
rm -rf $BASE_PATH/gitlab.com

#
# JJJ: de momento me he copiado (sin generarlo) los ficheros zz_generated_deepcopy.go y
#      zz_generated_defaulter.go. El segundo porque no se me genera, y el primero
#      porque se me genera sin algunas funciones que hacen falta
#
# # Generating just the deepcopy function
# "${CODEGEN_PKG}"/generate-groups.sh \
#   deepcopy-gen \
#   this_is_not_used \
#   "${BASE_PATH}/pkg/apis_aws" \
#   aws:v1beta2 \
#   --output-base "${BASE_PATH}" \
#   --go-header-file "${BASE_PATH}"/hack/boilerplate.go.txt
